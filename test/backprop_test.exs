defmodule BackpropTest do
  use ExUnit.Case

  # doctest Backprop

  setup do
    {:ok, [
      ann: Ann.newFeedforward([1, 2], [3], [2]),
      patterns: [
        { %{1 => 0.0, 2 => 1.0},  %{3 => 1.0} },
        { %{1 => 1.0, 2 => 0.0},  %{3 => 1.0} },
        { %{1 => 0.0, 2 => 0.0},  %{3 => 0.0} },
        { %{1 => 1.0, 2 => 1.0},  %{3 => 0.0} },
      ]
    ]}
  end

  test "XOR Backpropogation", context do
    {_time, {ann, data}} = :timer.tc(&Backprop.train/3, [context[:ann], fn _ ->
      Enum.take_random(context[:patterns], 2)
    end, %{learn_val: 5, terminate?: fn _, info -> info.adjError < 0.001 end}])

    # IO.write "(XOR Backprop time: #{time / 1_000_000}s, steps: #{data.step})"
    IO.write "(XOR Backprop: #{data.step})"
    Ann.saveGZ(ann, "backpropXOR.gz")

    sim = Ann.Simulation.new(ann, &Backprop.sigmoid/1)
    Enum.each context[:patterns], fn {inputs, outputs} ->
      output = Ann.Simulation.eval(sim, inputs).data[3]
      assert Float.round(output) == outputs[3], "For input #{inspect inputs}, expected close to #{outputs[3]}, got #{output}"
    end
  end
end
