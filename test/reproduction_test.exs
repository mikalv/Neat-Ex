defmodule ReproductionTest do
  use ExUnit.Case
  alias Ann.Connection
  alias Neat.Reproduction

  setup do
    neat = Map.put(Neat.new_single_fitness(Ann.new([1, 2, 3], [4], %{}), fn _ -> :rand.uniform end, population_size: 15, target_species_number: 3), :conn_id, 9)
    ann = Ann.newFeedforward([1], [2], [10])

    {:ok, [neat: neat, ann: ann]}
  end

  def conn_set(ann) do
    Enum.reduce Map.keys(ann.connections), MapSet.new, fn id, set ->
      MapSet.put(set, id)
    end
  end

  def find_connection(ann, input, output) do
    Enum.find Map.values(ann.connections), nil, fn conn ->
      conn.input == input and conn.output == output
    end
  end

  def get_attribute_counts(parent1, parent2, child) do
    Enum.reduce child.connections, {0, 0}, fn {id, conn}, {parent1_count, parent2_count} ->
      {
        (parent1_count + if parent1.connections[id] == conn, do: 1, else: 0),
        (parent2_count + if parent2.connections[id] == conn, do: 1, else: 0)
      }
    end
  end

  test "Reproduction.mate", _context do
    {bigger, smaller} = {Ann.newFeedforward([1], [2], [50, 1, 1]), Ann.newFeedforward([1], [2], [50])}
    extra = 9999
    smaller = Map.put(smaller, :connections, Map.put(smaller.connections, extra, Connection.new(1, 2, 1.0)))

    children = [equal_child, big_child, small_child] = [
      Reproduction.mate({bigger, 0}, {smaller, 0}),
      Reproduction.mate({bigger, 20}, {smaller, 0}),
      Reproduction.mate({bigger, 0}, {smaller, 20})
    ]

    Enum.each children, fn child -> #ensure it has qualities from both parents
      {big_count, small_count} = get_attribute_counts(bigger, smaller, child)
      assert big_count > 0
      assert small_count > 0
    end

    {big_set, small_set} = {conn_set(bigger), conn_set(smaller)}
    #ensure the topologies match what they should be
    assert MapSet.equal?(conn_set(equal_child), MapSet.union(big_set, small_set))
    assert MapSet.equal?(conn_set(big_child), big_set)
    assert MapSet.equal?(conn_set(small_child), small_set)
  end

  test "Reproduction.mutate", context do
    neat = context[:neat]
    ann = context[:ann]
    new_ann = Reproduction.mutate(ann, neat.opts)
    assert !Map.equal?(ann.connections, new_ann.connections)
  end

  test "Reproduction.mutate_new_node", context do
    neat = context[:neat]
    ann = context[:ann]
    new_ann = Reproduction.mutate_new_node(ann, neat.opts)

    [a, b] = MapSet.difference(conn_set(new_ann), conn_set(ann))
      |> Enum.map(fn id -> Map.fetch!(new_ann.connections, id) end)

    assert (a.output == b.input or b.output == a.input) and a.input != b.input #they should connect

    #find the overwritten connection
    {input, output} = if a.output == b.input, do: {a.input, b.output}, else: {b.input, a.output}
    disabled = find_connection(new_ann, input, output)
    assert disabled #ensure not nil
    refute disabled.enabled #ensure is false
  end

  test "Reproduction.mutate_new_link", context do
    neat = context[:neat]
    ann = context[:ann]
    new_ann = Reproduction.mutate_new_link(ann, neat.opts)

    [new_id] = MapSet.difference(conn_set(new_ann), conn_set(ann)) |> MapSet.to_list
    new_conn = Map.fetch!(new_ann.connections, new_id)
    #ensure no other connection like this existed previously
    refute find_connection(ann, new_conn.input, new_conn.output)
  end

  test "Reproduction.mutate_weights", context do
    neat = context[:neat]
    ann = context[:ann]
    new_ann = Reproduction.mutate_weights(ann, neat.opts)
    assert !Map.equal?(ann.connections, new_ann.connections) #different weights
    assert MapSet.equal?(conn_set(ann), conn_set(new_ann)) #same topology
  end
end
