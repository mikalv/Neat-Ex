defmodule Examples.FishSims.Utils do
  @moduledoc false
  def flatten(species) do
    Enum.flat_map(species, fn {_rep, members} -> members end)
  end

  #a - b, only with wrapping
  def closestDif(a, b, size) do
    Enum.min_by [a - b, (a + size) - b, (a - size) - b], &abs/1
  end

  def wrap(amt, limit) do
    cond do
      amt < -limit/2 -> wrap(amt + limit, limit)
      amt > limit/2 -> wrap(amt - limit, limit)
      :else -> amt
    end
  end
end
