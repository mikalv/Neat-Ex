defmodule Neat do
  @moduledoc """
  Neuro-Evolution of Augmenting Topologies ([NEAT](http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf)) is an algorithm for developing Artificial Neural Networks (ANNs) through the process of evolution.

  Neuro-Evolution, unlike back-propogation, easily allows the usage of recurrent neural networks instead of just feed-forward networks, and fitness functions instead of just training data. Additionally, since NEAT augments topologies, all the engine needs to start is the input/output layout, and a fitness function.

  Note: `new_single_fitness` is the simpler starting option. In this case, the fitness function is given an ANN, and returns the fitness. Parallell computation is handled automatically. `new_multi_fitness` allows more flexibility by providing the entire population of ANNs for evaluation all at once. See the `single_fitness_function` property listed [here](Neat.Options.html) for more details.

  For example usage and other information, see the [README.md](https://gitlab.com/onnoowl/Neat-Ex).
  """

  defstruct opts: nil, species: [], generation: 0, best: {nil, 0}

  alias Neat.Fitness
  alias Neat.PopulationManagement
  alias Neat.Reproduction
  alias Neat.Speciation

  @doc "Creates a new Neat given a Neat.Options"
  def new(opts) when is_map(opts) do
    %Neat{
      opts: opts
    }
  end

  @doc "Creates a new Neat (and a new Neat.Options) given a seed ANN (in which the initial population is based), a fitness_function (where single_fitness_function is true, see [Neat.Options](Neat.Options.html) for more info), and a keyword list of optional paramaters. See [Neat.Options](Neat.Options.html) for a list of paramaters and their defaults."
  def new_single_fitness(seed, fitness_function, opts_keyword_list \\ []) when is_function(fitness_function) do
    new(Neat.Options.new(seed, fitness_function, true, opts_keyword_list))
  end

  @doc "Creates a new Neat (and a new Neat.Options) given a seed ANN (in which the initial population is based), a fitness_function (where single_fitness_function is false, see [Neat.Options](Neat.Options.html) for more info), and a keyword list of optional paramaters. See [Neat.Options](Neat.Options.html) for a list of paramaters and their defaults."
  def new_multi_fitness(seed, fitness_function, opts_keyword_list \\ []) when is_function(fitness_function) do
    new(Neat.Options.new(seed, fitness_function, false, opts_keyword_list))
  end

  @doc false #Generates the initial population if the population is empty
  def evolve(neat = %Neat{species: [], opts: opts}) do
    seed = opts.seed
    {new_members, fun} = if Map.size(seed.connections) == 0 do
      {[], &Reproduction.mutate_new_link/2}
    else
      {[seed], &Reproduction.mutate/2}
    end
    new_members = Enum.map (length new_members)..(opts.population_size - 1), fn _ ->
      fun.(seed, neat.opts)
    end
    species = Enum.reduce new_members, [], fn member, species ->
      Speciation.speciateMember(member, species, neat.opts)
    end
    evolve(Map.put(neat, :species, species))
  end

  @doc "Steps the evolution process forward one step given a Neat struct. Returns the new Neat struct."
  def evolve(neat) do
    neat
      |> Fitness.map_fitness
      |> Fitness.assign_avg_fitness #for each species
      |> PopulationManagement.kill_lessers
      |> PopulationManagement.kill_dead_species
      |> PopulationManagement.store_best_ann
      |> PopulationManagement.randomize_rep
      |> Reproduction.repopulate
      |> Speciation.modify_compatibility_threshold
      |> Map.put(:generation, neat.generation + 1)
  end

  @doc "Repeatedly steps forward the evolution process until a given idealFitness is reached. If a print function is provided, it is passed the Neat struct each step for displaying the evolution process."
  def evolveUntil(neat = %Neat{generation: gen}, idealFitness, print \\ fn _ -> nil end) do
    if gen > 0, do: print.(neat)
    {_, fitness} = neat.best
    if fitness >= idealFitness do
      neat
    else
      evolveUntil(Neat.evolve(neat), idealFitness, print)
    end
  end

  @doc "Repeatedly steps forward the evolution process for a given number of generations. If a print function is provided, it is passed the Neat struct each step for displaying the evolution process."
  def evolveFor(neat, gensToRun, print \\ fn _ -> nil end)
  def evolveFor(neat = %Neat{generation: gen}, gensToRun, print) when gen < gensToRun do
    if gen > 0, do: print.(neat)
    evolveFor(Neat.evolve(neat), gensToRun, print)
  end
  def evolveFor(neat, _, print), do: (print.(neat); neat)

  @doc "Repeatedly steps forward the evolution process for a given number of minutes. If a print function is provided, it is passed the Neat struct each step for displaying the evolution process."
  def evolveForMins(neat, minsToRun, print \\ fn _ -> nil end, startTime \\ :erlang.monotonic_time(:seconds))
  def evolveForMins(neat = %Neat{generation: gen}, minsToRun, print, startTime) do
    if (:erlang.monotonic_time(:seconds) - startTime)/60 < minsToRun do
      if gen > 0, do: print.(neat)
      evolveForMins(Neat.evolve(neat), minsToRun, print, startTime)
    else
      (print.(neat); neat)
    end
  end
end
