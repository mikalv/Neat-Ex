defmodule Neat.Utils do
  @doc "Given the species list and a mapping function, each species is passed into the function in a new process, then the results are collected when all processes complete."
  def async_species_map(species, fun) do
    Async.async_map(species, fun)
  end

  def async_map(species, fun) do
    async_species_map species, fn {rep, members} ->
      {rep, Enum.map(members, fun)}
    end
  end

  def map(species, fun) do
    Enum.map species, fn {rep, members} ->
      {rep, Enum.map(members, fun)}
    end
  end

  def reduce(species, acc, fun) do
    Enum.reduce species, acc, fn {_, members}, acc ->
      Enum.reduce(members, acc, fun)
    end
  end

  @doc "Converts a species to a json, where each ANN's json is in the list under property \"species\" (in arbitrary order)"
  def toJson(species) do
    {:ok, json} = JSON.encode([species: Enum.flat_map(species, fn {_rep, members} ->
      Enum.map(members, fn ann -> Ann.breakdown(deconstruct(ann)) end)
    end)])
    json
  end

  @doc "Given either an `ann`, an `{ann, fitness}` tuple, or an `{ann, fitness, tsi}` tuple, ann is returned."
  def deconstruct({ann, _fitness, _tsi}), do: ann
  def deconstruct({ann, _fitness}), do: ann
  def deconstruct(ann), do: ann

  @doc "Uses SipHash to generate a (usually) unique 64 bit id given the provided term. The same term will always produce the same id."
  def gen_id(term) do
    SipHash.hash!("E14A9824D423B268", :erlang.term_to_binary(term)) #those bytes were chosen arbitrarily for the hasing function
  end
end
